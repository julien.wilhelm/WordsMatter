<?php

namespace WordsMatter;

class Init {

	public function __construct() {

		if(!is_dir(POSTS_DIR)) {
			mkdir(POSTS_DIR, 0777, true);
		} 
		if(!is_dir(GALLERY_DIR)) {
			mkdir(GALLERY_DIR . '/s/', 0777, true);
			mkdir(GALLERY_DIR . '/m/', 0777, true);
			mkdir(GALLERY_DIR . '/l/', 0777, true);
		
		}
		if(!file_exists(PUBLIC_POSTS_LIST)) {
			file_put_contents(PUBLIC_POSTS_LIST, '');
		}
		if(!file_exists(PRIVATE_POSTS_LIST)) {
			file_put_contents(PRIVATE_POSTS_LIST, '[]');
		}

		echo 'granted';
		
	}

}