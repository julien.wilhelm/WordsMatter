<?php 

header('Access-Control-Allow-Origin: *');
ini_set('log_errors', 1);

require './config.php';

if(isset($_POST['editorId']) && $_POST['editorId'] === EDITOR_ID) {
	
	$uri = $_SERVER['REQUEST_URI'];

	if(preg_match('/init\/?$/', $uri)) {

		require('./Init.php');
		new WordsMatter\Init;

	}

	else if(isset($_POST['action'])) {

		if(preg_match('/posts\/?$/', $uri)) {
		
			require('./Posts.php');
			$postManager = new WordsMatter\Posts();
			
			switch($_POST['action']) {

				case 'list':
					echo $postManager->listPosts();
					break;

				case 'get':
					if(isset($_POST['post'])) {
						echo $postManager->getPost($_POST['post']);
					}
					break;

				case 'push':
					if(isset($_POST['post'], $_POST['validation'])) {
						if($postManager->pushPost($_POST['post'])) {
							echo 'Post uploaded';
						}
					}
					break;

				case 'delete':
					if(isset($_POST['post'])) {
						if($postManager->deletePost($_POST['post'])) {
							echo 'Post deleted';
						}
					}
					break;
			
			}
			
		}

		else if(preg_match('/images\/?$/', $uri)) {
			
			require('./Images.php');
			$imageManager = new WordsMatter\Images();
			
			switch($_POST['action']) {

				case 'list':
					echo json_encode($imageManager->listImages());
					break;

				case 'push':
					if(isset($_FILES) && $_FILES['file']['size'] <= IMG_MAX_SIZE) {
						if($imageManager->pushImage($_FILES)) {
							echo 'Image uploaded';
						}
					}
					break;

				case 'delete':
					if(isset($_POST['image'])) {
						if($imageManager->deleteImage($_POST['image'])) {
							echo 'Image deleted';
						}
					}
					break;
					
			}

		}
		
	}
	
}