<?php 

/* Must be changed for production */
define('EDITOR_ID', 'makeMeAsComplexAsPossible'); // Equal to the id in 'app/assets/config.js'.
define('DOMAIN', 'http://dev.wordsmatter');

/* Possibly change */
define('POSTS_ORDER', 'recent-first'); // Other values are 'older-first' & 'last-modified'.
define('SITEMAP_DEFAULT_URLS', [ /* string collection */ ]); // A list of absolute URLs to append to the sitemap. 
	
/* Change with caution (required stuffs like files or dirs will be created by WordsMatter */

/* Directories*/
define('POSTS_DIR', '../blog/posts');
define('ABS_POSTS_DIR', DOMAIN . '/blog/posts');
define('GALLERY_DIR', '../blog/assets/images');
define('ABS_GALLERY_DIR', DOMAIN . '/blog/assets/images');
	
/* Files */
define('PRIVATE_POSTS_LIST', './index.json'); 
define('PUBLIC_POSTS_LIST', '../blog/index.html');
define('SITEMAP', '../blog/sitemap.xml'); 
define('INPUT_FILENAME', 'index.txt'); 
define('OUTPUT_FILENAME', 'index.html');

/* Gallery pressets */
define('IMG_MAX_SIZE', 1000000);
define('IMG_S_SIZE', 540); 
define('IMG_M_SIZE', 768); 
define('IMG_L_SIZE', 1024);