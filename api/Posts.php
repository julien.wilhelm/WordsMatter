<?php 

namespace WordsMatter;

class Posts {

	public function listPosts() {
		return file_get_contents(PRIVATE_POSTS_LIST);
	}

	public function getPost($post) {
		return file_get_contents(POSTS_DIR . '/' . $post . '/' . INPUT_FILENAME);
	}

	public function pushPost($post) {

		$post = json_decode($post);
		$postDirName = $this->slugify($post->title); // name-of-the-post
		$postDirPath = POSTS_DIR . '/' . $postDirName; // path/to/blog/name-of-the-post
		$postFilePath = $postDirPath . '/' . OUTPUT_FILENAME; // path/to/blog/name-of-the-post/index.html (by default)
		$postDraftPath = $postDirPath . '/' . INPUT_FILENAME; // path/to/blog/name-of-the-post/index.txt (by default)
	
		if($_POST['validation'] === 'false') {
			echo is_dir($postDirPath) ? 'update' : 'release'; 
			exit;
		}

		else if($_POST['validation'] === 'true') {

			// Save raw data for future edition (input).
			if(!is_dir($postDirPath)) {
				mkdir($postDirPath);
			}
			file_put_contents($postDraftPath, json_encode($post));

			// Build or remove public post (output).
			if($post->isDraft) {
				if(is_dir($postDirPath)) {
					if(is_file($postFilePath)) {
						unlink($postFilePath);
					}
				}
			}
			else {
				$output = (object) array(
					'date' => $post->date,
					'desc' => htmlspecialchars($post->desc),
					'title' => $post->title,
					'introduction' => $this->runEditor($post->introduction),
					'sections' => []
				);
				if($post->sections != null) {
					foreach ($post->sections as $section) {
						array_push($output->sections, 
							(object) array(
								'title' => $section->title,
								'content' => $this->runEditor($section->content)
							)
						);
					}
				}
				if(!is_dir($postDirPath)) {
					mkdir($postDirPath);
				}
				$this->exportPost($output, $postFilePath);
			}

			// Prepare data to update private index (JSON).
			$posts = json_decode(file_get_contents(PRIVATE_POSTS_LIST));
			for($i = 0, $l = count($posts); $i < $l; $i++) {
				if($posts[$i]->dir === $postDirName) {
					array_splice($posts, $i, 1);
					break;
				}
			}
			array_unshift($posts, 
			(object) array(
				'date' => $post->date,
				'title' => $post->title,
				'dir' => $postDirName,
				'isDraft' => $post->isDraft,
				)
			);
			switch(POSTS_ORDER) {
				case 'recent-first':
					usort($posts, function($a, $b) { return strcmp($b->date, $a->date); });
					break;
				case 'older-first':
					usort($posts, function($a, $b) { return strcmp($a->date, $b->date); });
					break;
				case 'last-modified':
				default:
					// Natural order.
					break;
			}
			
			$this->buildPrivateList($posts);
			$this->buildPublicList($posts);
			$this->buildSitemap($posts);

			return error_get_last() === null ? true : false;

		}

	}

	public function deletePost($post) {

		function removeDir($dir) {
			$items = array_diff(scandir($dir), ['..', '.']);
			foreach($items as $item) {
				$itemPath = $dir . '/' . $item;
				if(is_file($itemPath)) {
					unlink($itemPath);
				}
				else {
					removeDir($itemPath);	
				}
			}
			rmdir($dir);
		}

		$postDirName = POSTS_DIR . '/' . $post;

		if(is_dir($postDirName)) {
			removeDir($postDirName);
		}
		
		$posts = json_decode(file_get_contents(PRIVATE_POSTS_LIST));
		for($i = 0, $l = count($posts); $i < $l; $i++) {
			if($posts[$i]->dir === $_POST['post']) {
				array_splice($posts, $i, 1);
				break;
			}
		}

		$this->buildPrivateList($posts);
		$this->buildPublicList($posts);
		$this->buildSitemap($posts);

		return error_get_last() === null ? true : false;
	
	}

	private function runEditor($input) {

		$regex = [
			[ // <br>
				'desc' => '/\s{2}\n/',
				'output' => function ($ct) {
					return '<br/>';
				}
			],
			[ // Protect escaped underscores by convert them.
				'desc' => '/\\\_/',
				'output' => function () {
					return '```';
				}
			],
			[ // Only inline elements should be enclosed in a <p>.
				'desc' => '/^(?!3_|4_|5_|6_|_\s|__\s|\!\[[^\]]+\]\([^\)]+\)).+$/m',
				'output' => function ($ct) {
					return '<p>' . $ct[0] . '</p>';
				}
			],
			[ // <h3> / <h4> / <h5> / <h6>
				'desc' => '/^(3|4|5|6)_(?!\s).+/m', // n_title
				'output' => function ($ct) {
					return '<h' . $ct[0][0] . '>' . substr($ct[0], 2) . '</h' . $ct[0][0] . '>';
				} 
			],
			[ // <img> / <figure>
				'desc' => '/\!\[[^\]]+\]\([^\)]+\)/', // ![alt|legend](img_url)
				'output' => function ($ct) {
					
					$dt = explode('](', substr($ct[0], 2, -1));
					$inf = explode('|', $dt[0]);
					$url = $dt[1];
		
					$html = '';
					$html .= count($inf) > 1 ? '<figure>' : '';	
		
					// If from gallery, use responsive images.
					if(strpos($url, ABS_GALLERY_DIR) !== false) {
						$fn = explode('/', $url);
						$fn = $fn[(count($fn) -1)];
						$urls = array(
							's' => ABS_GALLERY_DIR . '/s/' . $fn, 
							'm' => ABS_GALLERY_DIR . '/m/' . $fn, 
							'l' => ABS_GALLERY_DIR . '/l/' . $fn
						);
						$html .= 
							'<picture>' .
								'<source srcset="' . $urls['l'] . '" media="(min-width:' . IMG_L_SIZE . 'px)">' .
								'<source srcset="' . $urls['m'] . '" media="(min-width:' . IMG_M_SIZE . 'px)">' .
								'<img loading="lazy" src="' . $urls['s'] . '" alt="' . $inf[0] . '" />' .
							'</picture>';
					}
		
					// Else, use external link as it.
					else { 
						$html .= '<img loading="lazy" src="' . $url . '" alt="' . $inf[0] . '" />';
					}
				
					$html .= count($inf) > 1 ? 
							'<figcaption>' .	 $inf[1] . '</figcaption>
						</figure>' : '';
		
					return $html;
		
				}
			],
			[ // <a>
				'desc' => '/\[[^\]]+\]\([^\)]+\)/', // [label](url)
				'output' => function ($ct) {
					$dt = explode('](', substr($ct[0], 1, -1));
					return '<a href="' . $dt[1] . '">' . $dt[0] . '</a>';
				}
			],
			[ // <ol>
				'desc' => '/(^__\s.*\n){1,}/m',
				'output' => function ($ct) {
					return '<ol>' . $ct[0] . '</ol>';
				}
			],
			[ // <li>
				'desc' => '/^(<ol>)?__\s.+/m', // __ ordered list item (the first element will be preceeding by <ol>)
				'output' => function ($ct) {
					return strpos($ct[0], '<ol>__') === 0 ?
						'<ol><li>' . substr($ct[0], 7) . '</li>' :
						'<li>' . substr($ct[0], 3) . '</li>';
				}
			],
			[ // <ul>
				'desc' => '/(^_\s.*\n){1,}/m',
				'output' => function ($ct) {
					return '<ul>' . $ct[0] . '</ul>';
				}
			],
			[ // <li>
				'desc' => '/^(<ul>)?_\s.+/m', // _ unordered list item (the first element will be preceeding by <ul>)
				'output' => function ($ct) {
					return strpos($ct[0], '<ul>_') === 0 ?
						'<ul><li>' . substr($ct[0], 6) . '</li>' :
						'<li>' . substr($ct[0], 2) . '</li>';
				}
			],
			[ // <strong>
				'desc' => '/(__)(?!\s)(.+?[_]*)\1/', // (space)__strong text__
				'output' => function ($ct) {
					return '<strong>' . substr($ct[0], 2, -2) . '</strong>';
				}
			],
			[ // <em>
				'desc' => '/(_)(?!\s)(.+?)\1/', // (space)_emphasic text_
				'output' => function ($ct) {
					return '<em>' . substr($ct[0], 1, -1) . '</em>';
				}
			],
			[ // (escaped underscores)
				'desc' => '/```/',
				'output' => function () {
					return '_';
				}
			]
		];

		$output = $input;
	
		// Striping source tags to prevent unexpected HTML.	
		$output = preg_replace('/(<([^>]+)>)/i', '', $output);

		for($i = 0, $l = count($regex); $i < $l; $i++) {
			$output = preg_replace_callback($regex[$i]['desc'], $regex[$i]['output'], $output);
		}
		
		return $output;
			
	}

	private function slugify($title) {
		$n = mb_strtolower($title);
		$n = str_replace([' ','à','á','â','ã','ä','ç','é','è','ê','ë','ì','í','î','ï','ò','ó','ô','õ','ö','š','ú','ù','û','ü','ý','ÿ','ž'], ['-','a','a','a','a','a','c','e','e','e','e','i','i','i','i','o','o','o','o','o','s','u','u','u','u','y','y','z'], $n);
		$n = str_replace([' ', '\'', '’'], ['-','-','-'], $n);
		$n = preg_replace('/[^a-zA-Z0-9\-]/', '', $n);
		$n = preg_replace('/\-{2,}/', '-', $n);
		$n = preg_replace('/^\-/', '', $n);
		$n = preg_replace('/\-$/', '', $n);
		return $n;
	}

	private function buildPrivateList($posts) {
		file_put_contents(PRIVATE_POSTS_LIST, json_encode($posts));
	}

	private function buildSitemap($posts) {

		$sm = '
			<?xml version="1.0" encoding="UTF-8"?>
			<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

		if(count(SITEMAP_DEFAULT_URLS) >= 1) {
			foreach(SITEMAP_DEFAULT_URLS as $url) {
				$sm .= '<url><loc>' . $url . '</loc></url>';
			}
		}

		if(count($posts) >= 1) {
			foreach($posts as $post) {
				if(!$post->isDraft) { 
					$url = ABS_POSTS_DIR . '/' . $post->dir . '/';
					$sm .= '<url><loc>' . $url . '</loc></url>';
				}
			}
		}

		$sm .= '</urlset>';

		$sm = str_replace(["\t", "\n"], ["", ""], $sm);
		file_put_contents(SITEMAP, $sm);

	}

	private function buildPublicList($posts) {

		$tpl = '
			<!DOCTYPE html>
			<html>
			<head>
				<meta charset="utf-8" />
				<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no" />
				<title>Blog</title>
			</head>
			<body>
				<main>
					<article>
						<ul>';
							if(count($posts) >= 1) {
								for($i = 0, $l = count($posts); $i < $l; $i++) {
									if(!$posts[$i]->isDraft) { 
										$tpl .= 
											'<li>
												<a href="' .  POSTS_DIR . '/' . $posts[$i]->dir . '">' . 
												$posts[$i]->title . ' 
												</a>
											</li>'; 
									}
								}
							}
						$tpl .= '
						</ul>
					</article>
				</main>
			</body>
			</html>';

		$tpl = str_replace(["\t", "\n"], ["", ""], $tpl);
		file_put_contents(PUBLIC_POSTS_LIST, $tpl);
	
	}

	private function exportPost($post, $postFilePath) {
	
		$tpl = '
			<!DOCTYPE html>
			<html>
			<head>
				<meta charset="utf-8" />
				<meta name="description" content="' . $post->desc . '" />
				<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no" />
				<title>' . $post->title . '</title>
			</head>
			<body>
				<main>
					<article>
						<h1>' . $post->title . '</h1>';
						if($post->introduction !== '') {
							$tpl .= '<div>' .  $post->introduction . '</div>';
						}
						for($i = 0, $l = count($post->sections); $i < $l; $i++) {
							$tpl .= '	
								<section>
									<h2>' . $post->sections[$i]->title . '</h2> 
									<div>' . $post->sections[$i]->content . '</div>
								</section>';
						}
						$tpl .= '
					</article>
				</main>
			</body>
			</html>';

		$tpl = str_replace(["\t", "\n"], ["", ""], $tpl);
		file_put_contents($postFilePath, $tpl);
	
	}

}