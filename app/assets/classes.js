'use strict';

// ghess (Get HTML Elements Short Syntax), by Awebsome.
const _ = {
	Q: (ref, root = document) => root.querySelector(ref),
	I: (ref, root = document) => root.getElementById(ref),
	C: (ref, root = document) => root.getElementsByClassName(ref),
	T: (ref, root = document) => root.getElementsByTagName(ref)
};

// chess (Create HTML Elements Short Syntax), by Awebsome.
function chess(obj) { 
	const elm = document.createElement(obj.type); 
	if(obj.text) {
		elm.textContent = obj.text;
	}
	else if(obj.html) {
		elm.innerHTML = obj.html;
	}
	if(obj.attributes) {
		for(const attribute in obj.attributes) {
			elm.setAttribute(attribute, obj.attributes[attribute]);
		}
	}
	if(obj.events) {
		for(const event of obj.events) {
			elm.addEventListener(event.type, event.function);
		}
	}
	if(obj.children) {
		for(const child of obj.children) {
			elm.appendChild(chess(child));
		}
	}
	return elm;
}

class Model {

	static reachApi(endpoint, args, callback) {

		const formData = new FormData();
		formData.append('editorId', EDITOR_ID);
		if(args !== null) {
			for(const arg of args) {
				formData.append(arg.name, arg.value);
			}
		}

		const req = new XMLHttpRequest();
		req.open('POST', API_URL + endpoint, true);
		req.onload = () => callback(req.responseText);
		req.send(formData);

	}

	static getCurrentPost() {
				
		const post = {
			date: _.I('post-date').value,
			isDraft: _.I('post-draft').checked,
			desc: _.I('in-desc').value,
			title: _.I('in-h1').value,
			introduction: _.I('in-intro').value,
			sections: null
		};
		
		const secElms = _.C('in-sec');
		if(secElms.length > 0) {
			post.sections = [];
			for(const secElm of secElms) {
				post.sections.push(
					{
						title: _.Q('input', secElm).value,
						content: _.Q('textarea', secElm).value,
					}
				);
			}
		}
		
		return post;
		
	}

}

class View {

	static setViewportHeight() { 
		UI.main.style.height = (window.innerHeight - 70) + 'px';
	}

	static toggleTheme() {
		UI.body.classList.contains('--dark') ? 
			View.toLightTheme(): 
			View.toDarkTheme();
	}

	static toDarkTheme() {	
		UI.body.classList.add('--dark');
		localStorage.setItem('theme', 'dark');
	}

	static toLightTheme() {
		UI.body.classList.remove('--dark');
		localStorage.setItem('theme', 'light');
	}

	static toggleLayout() {
		UI.main.classList.contains('--alternate') ? 
			View.toColumnLayout():
			View.toRowLayout();
	}

	static toColumnLayout() {	
		UI.main.classList.remove('--alternate');
		localStorage.removeItem('layout');
	}

	static toRowLayout() {	
		UI.main.classList.add('--alternate');
		localStorage.setItem('layout', 'alternate');
	}

	static setNotice(content) {
		UI.notice.setAttribute('class', '');
		void UI.notice.offsetWidth;			
		UI.notice.textContent = content;
		UI.notice.setAttribute('class', 'notice --visible');
		View.setDial();
	}

	static releaseTemplate = `
	<h2>${LAB.bt.pushPost}</h2>
	<p>${LAB.dial.confPushSv}</p>
	<button class="trap-last" onclick="ApiController.pushPost(true)">${LAB.bt.confirm}</button>`;

	static pushTemplate = `
	<h2>${LAB.bt.pushPost}</h2>
	<p>${LAB.dial.confUpdateSv}</p>
	<button class="trap-last" onclick="ApiController.pushPost(true)">${LAB.bt.update}</button>`;

	static setDial(content) {
		if(content !== undefined) {
			_.Q('div', UI.dial).innerHTML = content;
			UI.dial.classList.add('--visible');
			let elm;
			if(elm = _.Q('.dial-content input')) {
				elm.focus();
			}
			else if(elm = _.Q('.dial-content textarea')) {
				elm.focus();
			}
			else if(elm = _.Q('.dial-content button')) {
				elm.focus();
			}	
		}
		else {
			UI.dial.classList.remove('--visible');
		}
	}
	
	static setImagesList(data) {

		let dialBody = `
			<h2>${LAB.bt.getImages}</h2>
			<label>
				<h3>${LAB.bt.addImg}</h3>
				<input 
					accept="image/jpeg, image/png, image/webp"
					class="${data === '[]' ? 'trap-last' : '' }"
					onchange="ApiController.pushImage(this.files[0])"
					type="file" />
			</label>
			${LAB.bt.condImg}`;
	
		if(data !== '[]') {			
			const images = JSON.parse(data);
			dialBody += `
			<h3>${LAB.bt.availImg}</h3>
			<div class="gallery">
				<ul class="getimages-ul">`;
			for(let i = 0, l = images.length; i < l; i++) {
				dialBody += ` 
					<li>
						<h4>${images[i].name}</h4>
						<button 
							class="--danger"
							onclick="ApiController.deleteImage('${images[i].name}')"
							title="${LAB.bt.delete}">
							X
						</button>
						<button
							onclick="window.open('${images[i].lImgPath}')">
							${LAB.bt.see}
						</button>
						<button
							class="${i === (l - 1) ? 'trap-last' : '' }" 
							onclick="View.insertImage('${images[i].sImgPath}')">
							${LAB.bt.add}
						</button>
						<img loading="lazy" src="${images[i].sImgPath}" />
						<p><b>URL</b> ${images[i].sImgPath}</p>
					</li>`;
			}
			dialBody += `
				</ul>
			</div>`;
		}
	
		View.setDial(dialBody);
	
	}

	static setPostsList(data) {

		const posts = JSON.parse(data);	

		if(posts.length > 0) {

			let dialBody = `
				<h2>${LAB.bt.listPost}</h2>
				<p>${LAB.dial.editPost}</p>
				<ul class="getposts-ul">`;

			for(let i = 0, l = posts.length; i < l; i++) {
				dialBody += `	
					<li>
						<button
							class="--danger"
							onclick="ApiController.deletePost(\'${posts[i].dir}\')"
							title="${LAB.bt.delete}">
							X
						</button>
						<button 
							class="${i === (l - 1) ? 'trap-last' : '' }"
							onclick="ApiController.getPost(\'${posts[i].dir}\')"
							title="${LAB.bt.open}"
							value="${posts[i].dir}">
							${posts[i].title} 
							${posts[i].isDraft ? 
								`<span class="private">${LAB.bt.private}</span>` : 
								`<span class="published">${LAB.bt.published}</span>`
							}
						</button>
					</li>`;
			}
			dialBody += '</ul>';
			View.setDial(dialBody);
		}

		else {
			View.setNotice(LAB.notice.svEmpty);
		}

	}

	static runEditor(inputId) {

		let inputElm = _.I(inputId);
		let outputElm = _.I(inputId.replace('in', 'out')); 
		let content = inputElm.value.replace(/(<([^>]+)>)/ig, '');	

		if(inputElm.type === 'textarea') {
			for(const regex of REGEX) {
				content = content.replace(regex.desc, regex.output);
			}
		}

		outputElm.innerHTML = content;

	}

	static setPost(input) {

		try {

			const post = JSON.parse(input);
			View.unsetPost();

			_.I('post-date').value = post.date;
			_.I('post-draft').checked = post.isDraft;	
			_.I('in-h1').value = post.title;
			_.I('in-desc').value = post.desc,
			_.I('in-intro').value = post.introduction;
			View.runEditor('in-h1');		
			View.runEditor('in-intro');

			if(post.sections) {
				for(const sec of post.sections) {
					const currentMarker = marker;
					_.I('bt-add-section').click();
					_.I('in-sec-h2-' + currentMarker).value = sec.title;
					View.runEditor('in-sec-h2-' + currentMarker);
					_.I('in-sec-content-' + currentMarker).value = sec.content;
					View.runEditor('in-sec-content-' + currentMarker);
				}
			}

			View.setNotice(LAB.notice.reloaded);

		}

		catch(e) {
			View.setNotice(LAB.notice.error);
		}
		
	}

	static unsetPost() {

		_.I('post-date').value = '';
		_.I('post-draft').checked = false;	
		_.I('in-h1').value = '';
		_.I('in-desc').value = '',
		_.I('in-intro').value = '';
		View.runEditor('in-h1');
		View.runEditor('in-intro');

		const secElms = _.C('in-sec');
		while(secElms.length >= 1) {
			const secElm = secElms[0]; 
			secElms[0].remove(); // Remove input.
			_.I(secElm.id.replace('in', 'out')).remove(); // Remove output.
		}

	}

	static addSection() {

		let currentMarker = marker;

		UI.output.appendChild(
			chess({
				type: 'section',
				attributes: {
					id: 'out-sec-' + marker
				},
				children: [
					{ 
						type: 'h2',
						attributes: {
							id: 'out-sec-h2-' + marker
						}
					},
					{ 
						type: 'div',
						attributes: {
							id: 'out-sec-content-' + marker
						}
					}
				]
			})
		);

		let inputSecElm = chess({
			type: 'section',
			attributes: {
				class: 'in-sec',
				id: 'in-sec-' + marker
			}
		});

		for (let elmToCreate of [
			{ type: 'input', id: 'in-sec-h2-' + marker, placeholder: LAB.input.h2 },
			{ type: 'textarea', id: 'in-sec-content-' + marker, placeholder: LAB.input.secContent }

		]){
			inputSecElm.appendChild(
				chess({
					type: 'label',
					text: elmToCreate.placeholder,
					attributes: {
						for: elmToCreate.id,
					}
				})
			);
			inputSecElm.appendChild(
				chess(
					{
						type: elmToCreate.type,
						attributes: {
							id: elmToCreate.id,
							onfocus: 'lastActiveSec = this.parentNode',
							oninput: 'View.runEditor(this.attributes.id.value)',	
							placeholder: elmToCreate.placeholder,
							title: elmToCreate.placeholder,
							type: 'text'
						},
						
					}
				)
			);

		}

		// Quick insertion for titles.
		for(let i = 3; i <= 6; i++) {
			inputSecElm.appendChild(
				chess({
					type: 'button',
					text: 'h' + i,
					attributes: {
						title: LAB.bt.addHx + i
					},
					events: [
						{
							type: 'click',
							function: () => View.formatContent(_.Q('textarea', inputSecElm), { title: LAB.bt.addHx + i, tag: 'h' + i })
						}
					]
				})
			);
		}

		// Quick insertion for strong, em, ol, ul, a, img, figure.
		for(const elmToCreate of [
			{ text: 'str', title: LAB.bt.addStr, tag: 'strong' },
			{ text: 'em', title: LAB.bt.addEm, tag: 'em' },
			{ text: 'ol', title: LAB.bt.addOl, tag: 'ol' },
			{ text: 'ul', title: LAB.bt.addUl, tag: 'ul' },
			{ text: 'a', title: LAB.bt.addA, tag: 'a' },
			{ text: 'img', title: LAB.bt.addImg, tag: 'img' },
			{ text: 'fig', title: LAB.bt.addFig, tag: 'figure' }
		]) {
			inputSecElm.appendChild(
				chess({
					type: 'button',
					text: elmToCreate.text,
					attributes: {
						title: elmToCreate.title
					},
					events: [
						{
							type: 'click',
							function: () => View.formatContent(_.Q('textarea', inputSecElm), elmToCreate)
						}
					]
				})
			);
		}

		// Del a section.
		inputSecElm.appendChild(
			chess({
				type: 'button',
				text: 'X',
				attributes: {
					class: '--danger',
					title: LAB.bt.delSec
				},
				events: [
					{
						type: 'click',
						function: () => {
							if(confirm(LAB.dial.confDelSec)) {
								_.I('in-sec-' + currentMarker).remove();
								_.I('out-sec-' + currentMarker).remove();
							}
						}
					}
				]
			})
		);

		_.Q('.content').appendChild(inputSecElm);
		
		// Scroll down and focus to the new section.
		document.location.replace(document.location.pathname + '#in-sec-h2-' + marker);
		inputSecElm.children[1].focus();
	
		marker++;

	}

	static formatContent(targetElm, object) {

		// Get user input from highlighted text ('' if empty).
		const input = targetElm.value.substring(targetElm.selectionStart, targetElm.selectionEnd);
		const urlRegex = /^https?/i;

		let dialBody = `<form><h2>${object.title}</h2>`;

		switch(object.tag) {

			case 'h3': 
			case 'h4': 
			case 'h5': 
			case 'h6':
				dialBody +=
					`<label>
						${LAB.input.txtToTransf}
						<input focus value="${input}" required />
					</label>`;
				break;

			case 'strong': 
			case 'em':
				dialBody +=
					`<label>
						${LAB.input.txtToTransf}
						<input value="${input}" required />
					</label>`;
				break;

			case 'ol': 
			case 'ul':
				dialBody +=
					`<label>
						${LAB.input.list}
						<textarea required>${input}</textarea>
					</label>`;
				break;

			case 'a':
				dialBody +=
					`<label>
						${LAB.input.url}
						<input placeholder="https://example.com" value="${input.match(urlRegex) != null ? input : ''}" required />
					</label>
					<label>
						${LAB.input.lab}
						<input value="${input.match(urlRegex) != null ? '' : input}" required />
					</label>`;
				break;

			case 'img':
				dialBody +=
					`<label>
						${LAB.input.url}
						<input placeholder="https://example.com/image.png" value="${input.match(urlRegex) != null ? input : ''}" required />		
					</label>
					<label>
						${LAB.input.imgAlt}
						<input value="${input.match(urlRegex) != null ? '' : input}" required />
					</label>`;
				break;

			case 'figure':
				dialBody +=
					`<label>
						${LAB.input.url}
						<input placeholder="https://example.com/image.png" value="${input.match(urlRegex) != null ? input : ''}" required />
					</label>
					<label>
						${LAB.input.imgLeg}
						<input value="${input.match(urlRegex) != null ? '' : input}" required />
					</label>
					<label>
						${LAB.input.imgAlt}
						<input required />
					</label>`;
				break;

		}

		dialBody += `<button class="trap-last" type="submit">${LAB.bt.confirm}</button></form>`;
		View.setDial(dialBody);

		// Build the corresponding output when the form is submitted.
		_.Q('.dial form').onsubmit = e => {

			let output;

			switch(object.tag) {

				case 'h3': 
				case 'h4': 
				case 'h5': 
				case 'h6':
					output = `\n${object.tag.substring(1,2)}_${e.target.elements[0].value}\n`;
					break;

				case 'strong': 
					output = `__${e.target.elements[0].value}__`;
					break;

				case 'em':
					output = `_${e.target.elements[0].value}_`;
					break;

				case 'ol':
				case 'ul':
					const nScore = object.tag === 'ol' ? '__' : '_'; 
					output = '\n';
					for(let elm of e.target.elements[0].value.split('\n')) { 
						if(elm.trim() !== '') {
							output += `${nScore} ${elm.trim()}\n`; 
						}
					}
					break;

				case 'a':
					output = `[${e.target.elements[1].value}](${e.target.elements[0].value})`;
					break;

				case 'img':
					output = `![${e.target.elements[1].value}](${e.target.elements[0].value})`;
					break;

				case 'figure':
					output = `![${e.target.elements[2].value}|${e.target.elements[1].value}](${e.target.elements[0].value})`;
					break;

			}

			// Replace input by output.
			targetElm.value = 
				targetElm.value.substring(0, targetElm.selectionStart) +
				output + 
				targetElm.value.substring(targetElm.selectionEnd);

			// Actualize.
			e.preventDefault();
			View.runEditor(targetElm.id);
			targetElm.focus();
			View.setDial();

		};

	}

	static insertImage(picPath) {

		View.setDial();

		const target = _.Q('textarea', lastActiveSec);
		target.value += `![alternative](${picPath})`;
		View.runEditor(target.id);

	}

}

class LocalController {

	static pushPost() {
		const data = JSON.stringify(Model.getCurrentPost());
		localStorage.setItem('post', data);
		localStorage.getItem('post') === data ?
			View.setNotice(LAB.notice.wsSaved):
			View.setNotice(LAB.notice.error);
	}

	static deletePost() {
		if(localStorage.getItem('post')) {	
			if(confirm(LAB.dial.confDelWs)) {
				localStorage.removeItem('post');
				if(!localStorage.getItem('post')) {
					View.unsetPost();
					View.setNotice(LAB.notice.wsDeleted);
				}
				else {
					View.setNotice(LAB.notice.error);
				}
			}
		}
		else {
			View.setNotice(LAB.notice.wsEmpty);
		}
	}

}

class FileController {

	static getPost(file) {
		const reader = new FileReader();		
		reader.onloadend = e => View.setPost(e.target.result);
		reader.readAsText(file);
	}

	static pushPost() {
		View.setDial(`
			<h2>${LAB.bt.exportPost}</h2>
			<p>${LAB.dial.exportPost}</p>
			<ul class="exportpost-ul">
				<li><pre>${JSON.stringify(Model.getCurrentPost())}</pre></li>
			</ul>
			<button class='trap-last' onclick='View.setDial()'>${LAB.bt.close}</button>`
		);
	}
	
}

class ApiController {

	static listPosts() {
		Model.reachApi(
			'/posts',
			[
				{ 
					name: 'action', 
					value: 'list'
				}
			],
			data => View.setPostsList(data)
		);
	}
	
	static getPost(post) {
		Model.reachApi(
			'/posts',
			[
				{ 
					name: 'action', 
					value: 'get'
				},
				{
					name: 'post', 
					value: post
				}
			],
			data => View.setPost(data)
		);
	}

	static pushPost(validation = false) {
		if(_.I('out-h1').textContent.trim() != '') { 
			Model.reachApi(
				'/posts',
				[
					{
						name: 'action', 
						value: 'push'
					},
					{
						name: 'post', 
						value: JSON.stringify(Model.getCurrentPost())
					}, 
					{
						name: 'validation', 
						value: validation
					}
				],
				mess => {
					switch(mess) {
						case 'release':
							View.setDial(View.releaseTemplate);
							break;
						case 'update':
							View.setDial(View.pushTemplate);
							break;
						case 'Post uploaded':
							View.setNotice(LAB.notice.svPushed);
							break;
						default:
							View.setNotice(LAB.notice.error);
					}
				}
			);
		}
		else {
			View.setNotice(LAB.notice.svUntitled);
		}
	}

	static deletePost(post) {
		if(confirm(LAB.dial.confDelSv)) {
			Model.reachApi(
				'/posts',
				[
					{
						name: 'action', 
						value: 'delete'
					},
					{
						name: 'post', 
						value: post
					}
				],
				mess => {
					if(mess === 'Post deleted')	{
						ApiController.listPosts();
					}
					else {
						View.setNotice(LAB.notice.error);
					}
				}
			);
		}
	}

	static listImages() {
		Model.reachApi(
			'/images',
			[
				{
					name: 'action',
					value: 'list'
				}
			],
			data => View.setImagesList(data)
		);		
	}
	
	static pushImage(file) {
		Model.reachApi(
			'/images',
			[
				{ 
					name: 'action', 
					value: 'push'
				},
				{ 
					name: 'file',
					value: file
				}
			],
			mess => mess === 'Image uploaded' ? 
				ApiController.listImages():
				View.setNotice(LAB.notice.error)
		);
	}

	static deleteImage(image) {
		if(confirm(`${LAB.bt.delete} ${image} ?`)) {
			Model.reachApi(
				'/images',
				[
					{ 
						name: 'action', 
						value: 'delete' 
					},
					{ 
						name: 'image', 
						value: image 
					}
				],
				mess => mess === 'Image deleted' ? 
					ApiController.listImages():
					View.setNotice(LAB.notice.error)
			);
		}
	}

}