'use strict';

const LAB = {};

if(LANG === 'fr') {
	LAB.meta = {
		desc : 'Rédigez depuis l\'Application ; partagez grâce à l\'API connectée. WordsMatter est un module de gestion de blog 100% autonome, libre et gratuit. Son efficience en fait une alternative écoresponsable aux Systèmes de Gestion de Contenu traditionnels (CMS).',
		title :'WordsMatter | Écrivez ici, publiez ailleurs.'
	};
	LAB.bt = {
		add: 'Ajouter',
		addA: 'Lien hypertexte',
		addEm: 'Texte emphasique',
		addFig: 'Figure',
		addHx: 'Sous-titre de niveau ',
		addImg: 'Image',
		addOl: 'Liste ordonnée',
		addSec: 'Ajouter une section',
		addStr: 'Texte important',
		addUl: 'Liste non ordonnée',
		availImg: 'Image(s) disponible(s)',
		close: 'Fermer',
		condImg: 'Formats acceptés : JPEG, PNG, WEBP.<br/>Taille de fichier maximale : 1Mo.',
		confirm: 'Confirmer',
		delSec: 'Supprimer la section',
		delete: 'Supprimer',
		doc: 'Documentation',
		exportPost: 'Exporter vers fichier',
		getImages: 'Gérer la galerie',
		listPost: 'Modifier des posts depuis le blog',
		loadPost: 'Importer depuis fichier',
		open: 'Ouvrir',
		pickImg: 'Ou choisissez-en une ici',
		private: 'privé',
		published: 'publié',
		pushPost: 'Envoyer le post vers le blog',
		save: 'Sauvegarder',
		see: 'Voir',
		toggleLayout: 'Changer la disposition de l\'application',
		toggleTheme: 'Changer le thème de l\'application',
		update: 'Mettre à jour',
	};
	LAB.input = {
		date: 'Publié le',
		desc: 'Meta description',
		draft: 'Brouillon ?',
		h1: 'Titre du post (h1)',
		h2: 'Titre de section (h2)',
		imgAlt: 'Description (alternative)',
		imgLeg: 'Légende de l\'image',
		lab: 'Libellé',
		list: 'Eléments composant la liste (un par ligne)',
		secContent: 'Contenu de section',
		secIntro: 'Introduction (optionnelle)',
		txtToTransf: 'Texte à transformer',
		url: 'Adresse (URL)'
	};
	LAB.notice = {
		error: 'Une erreur est survenue...',
		reloaded: 'Post rechargé.',
		svPushed: 'Post envoyé vers le blog !',
		svEmpty: 'Il n\'y a encore rien sur le blog...',
		svUntitled: 'Votre post doit avoir un titre !',
		wsDeleted: 'Post supprimé du navigateur !',
		wsEmpty: 'Rien à supprimer ici...',
		wsSaved: 'Post enregistré dans le navigateur.'
	};
	LAB.dial = {
		confDelSec: 'Supprimer cette section ?',
		confDelSv: 'Supprimer le post sélectionné du blog ?',
		confDelWs: 'Supprimer le post de votre navigateur ?',
		confPushSv: 'Envoyer le contenu vers le blog ?',
		confUpdateSv: 'Un post avec un titre identique existe déjà sur le blog.',
		editPost: 'Quel post souhaitez-vous modifier depuis le blog ?',
		exportPost: 'Copiez-collez le contenu suivant dans un fichier. Utilisez l\'option <i>Importer depuis fichier</i> accessible depuis le menu pour le recharger.',
	};
}

else {
	LAB.meta = {
		desc : 'Write from the web application; share with the API. WordsMatter is a 100% autonomous, open-source and free blog management module. Its efficiency makes it an eco-responsible alternative to traditional content management systems (CMS).',
		title :'WordsMatter | Write here, publish elsewhere.'
	};
	LAB.bt = {
		add: 'Add',
		addA: 'Hyperlink',
		addEm: 'Emphasic text ',
		addFig: 'Figure',
		addHx: 'Subtitle of level ',
		addImg: 'Image',
		addOl: 'Ordered list ',
		addSec: 'Add a section',
		addStr: 'Strong text',
		addUl: 'Unordered list',
		availImg: 'Available image(s)',
		close: 'Close',
		condImg: 'Formats accepted: JPEG, PNG, WEBP.<br/>Maximum file size: 1Mb.',
		confirm: 'Confirm',
		delSec: 'Delete the section',
		delete: 'Delete',
		doc: 'Documentation',
		exportPost: 'Export to file',
		getImages: 'Manage gallery',
		listPost: 'Edit from the blog',
		loadPost: 'Import from file',
		open: 'Open',
		pickImg: 'Or pick one here',
		private: 'private',
		published: 'published',
		pushPost: 'Push to the blog',
		save: 'Save',
		see: 'See',
		toggleLayout: 'Change the app layout', 
		toggleTheme: 'Change the app theme',
		update: 'Update',
	};
	LAB.input = {
		date: 'Publication date',
		draft: 'Draft ?',
		desc: 'Meta description',
		h1: 'Title of the post (h1)',
		h2: 'Title of the section (h2)',
		imgAlt: 'Description (alternative)',
		imgLeg: 'Image caption',
		lab: 'Label',
		list: 'Elements making up the list (one by line)',
		secContent: 'Content of the section',
		secIntro: 'Introduction (optional)',
		txtToTransf: 'Text to transform',
		url: 'Address (URL)'
	};
	LAB.notice = {
		error: 'An error has occurred...',
		reloaded: 'Post reloaded.',
		svPushed: 'Post sent on the blog !',
		svEmpty: 'There is nothing on the blog yet.',
		svUntitled: 'Your post mush have a title !',
		wsDeleted: 'Post deleted from your web browser !',
		wsEmpty: 'Nothing to delete here...',
		wsSaved: 'Post saved in your web browser.'
	};
	LAB.dial = {
		confDelSec: 'Delete this section?',
		confDelSv: 'Delete the selected post from the blog?',
		confDelWs: 'Delete the post from your browser?',
		confPushSv: 'Send content to the blog?',
		confUpdateSv: 'A post with an identical title already exists on the blog.',
		editPost: 'Which post do you want to edit from the blog?',
		exportPost: 'Copy and paste the following content into a file. Use the <i>Import from file</i> option accessible from the menu to reload it.',
	}
}