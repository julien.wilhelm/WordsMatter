const EDITOR_ID = 'makeMeAsComplexAsPossible'; // Equal to the id specified in 'api/config.php'.
const API_URL = '../api'; // Use a name as complex as possible to avoid brute force attacks.
const LANG = 'fr'; // Available: fr, en.

// Go further: https://awebsome.fr/blog-awebsome/creer-un-blog-ecoresponsable-avec-wordsmatter/ (fr)