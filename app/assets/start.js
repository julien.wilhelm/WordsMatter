'use strict';

let marker = 0;
let lastActiveSec;

// Starting UI.
const UI = {};
UI.body = document.body;
UI.main = chess({ type: 'main' });

UI.notice = chess({
	type: 'p',
	attributes: {
		class: 'notice'
	},
});

UI.dial = chess({
	type: 'div',
	attributes: {
		class: 'dial',
	},
	children: [
		{
			type: 'button',
			attributes: {
				class: 'trap-first',
				onclick: 'View.setDial()'
			},
			text: LAB.bt.close 
		},
		{
			type: 'div',
			attributes: {
				class: 'dial-content'
			}
		}
	]
});

UI.nav = chess({
	type: 'nav',
	attributes: {
		class: 'nav',
	}, 
	children: [
		{
			
			type: 'button',
			attributes: {
				class: "ws-required",
				onclick: "LocalController.pushPost()",
				title: LAB.bt.save
			},
			html: '<svg viewBox="-3 -3 30 30"><path d="M13 3h2.996v5h-2.996v-5zm11 1v20h-24v-24h20l4 4zm-17 5h10v-7h-10v7zm15-4.171l-2.828-2.829h-.172v9h-14v-9h-3v20h20v-17.171z"/></svg>' 
		},
		{
			type: 'button',
			attributes: {
				class: "ws-required",
				onclick: "LocalController.deletePost()",
				title: LAB.bt.delete
			},
			html: '<svg viewBox="3 3 18 18"><path d="m10.586 12-4.9497 4.9497 1.4142 1.4142 4.9497-4.9497 4.9497 4.9497 1.4142-1.4142-4.9497-4.9497 4.9497-4.9497-1.4142-1.4142-4.9497 4.9497-4.9497-4.9497-1.4142 1.4142z" /></svg>'
		},
		{
			type: 'span', 
			attributes: {
				class: 'separator'
			}
		},
		{
			type: 'button',
			attributes: {
				class: "serv-required",
				onclick: "ApiController.pushPost()",
				title: LAB.bt.pushPost
			},
			html: '<svg viewBox="0 0 24 24"><path d="M16 16h-3v5h-2v-5h-3l4-4 4 4zm3.479-5.908c-.212-3.951-3.473-7.092-7.479-7.092s-7.267 3.141-7.479 7.092c-2.57.463-4.521 2.706-4.521 5.408 0 3.037 2.463 5.5 5.5 5.5h3.5v-2h-3.5c-1.93 0-3.5-1.57-3.5-3.5 0-2.797 2.479-3.833 4.433-3.72-.167-4.218 2.208-6.78 5.567-6.78 3.453 0 5.891 2.797 5.567 6.78 1.745-.046 4.433.751 4.433 3.72 0 1.93-1.57 3.5-3.5 3.5h-3.5v2h3.5c3.037 0 5.5-2.463 5.5-5.5 0-2.702-1.951-4.945-4.521-5.408z"/></svg>'
		},
		{
			type: 'button',
			attributes: {
				class: "serv-required",
				onclick: "ApiController.listPosts()",
				title: LAB.bt.listPost
			},
			html: '<svg viewBox="0 0 24 24"><path d="M8 20h3v-5h2v5h3l-4 4-4-4zm11.479-12.908c-.212-3.951-3.473-7.092-7.479-7.092s-7.267 3.141-7.479 7.092c-2.57.463-4.521 2.706-4.521 5.408 0 3.037 2.463 5.5 5.5 5.5h3.5v-2h-3.5c-1.93 0-3.5-1.57-3.5-3.5 0-2.797 2.479-3.833 4.433-3.72-.167-4.218 2.208-6.78 5.567-6.78 3.453 0 5.891 2.797 5.567 6.78 1.745-.046 4.433.751 4.433 3.72 0 1.93-1.57 3.5-3.5 3.5h-3.5v2h3.5c3.037 0 5.5-2.463 5.5-5.5 0-2.702-1.951-4.945-4.521-5.408z"/></svg>'
		},
		{
			type: 'button',
			attributes: {
				class: "serv-required",
				onclick: "ApiController.listImages()",
				title: LAB.bt.getImages
			},
			html: '<svg viewBox="-2 -2 27 27"><path d="M1.859 6l-.489-2h21.256l-.491 2h-20.276zm1.581-4l-.439-2h17.994l-.439 2h-17.116zm20.56 16h-24l2 6h20l2-6zm-20.896-2l-.814-6h19.411l-.839 6h2.02l1.118-8h-24l1.085 8h2.019zm2.784-3.995c-.049-.555.419-1.005 1.043-1.005.625 0 1.155.449 1.185 1.004.03.555-.438 1.005-1.044 1.005-.605 0-1.136-.449-1.184-1.004zm7.575-.224l-1.824 2.68-1.813-1.312-2.826 2.851h10l-3.537-4.219z"/></svg>'
		},
		{
			type: 'span', 
			attributes: {
				class: 'separator'
			}
		},
		{
			type: 'button',
			attributes: {
				onclick: "FileController.pushPost()",
				title: LAB.bt.exportPost
			},
			html: '<svg viewBox="1 1 21 21"><path d="M10.4971 12.9823L10 12.4853L12.4853 10L14.9706 12.4853L14.4735 12.9823L12.8368 11.3456V16H12.1338V11.3456L10.4971 12.9823Z" /><path d="M15.1571 3H6.63636C5.73262 3 5 3.73262 5 4.63636V19.3636C5 20.2674 5.73262 21 6.63636 21H18.0909C18.9946 21 19.7273 20.2674 19.7273 19.3636V7.57019L15.1571 3ZM6.63636 4.63636H13.1818V7.90909C13.1818 8.81283 13.9144 9.54545 14.8182 9.54545H18.0909V19.3636H6.63636V4.63636ZM14.8182 7.90909V4.97527L17.752 7.90909H14.8182Z" /></svg>'
		},
		{
			type: 'label',
			attributes: {
				id: 'label-open-file',
				title: LAB.bt.loadPost
			},
			html: '<svg viewBox="1 1 21 21"><path d="M14.4735 13.0177L14.9706 13.5147L12.4853 16L10 13.5147L10.4971 13.0177L12.1338 14.6544V10H12.8367V14.6544L14.4735 13.0177Z" /><path d="M15.1571 3H6.63636C5.73262 3 5 3.73262 5 4.63636V19.3636C5 20.2674 5.73262 21 6.63636 21H18.0909C18.9946 21 19.7273 20.2674 19.7273 19.3636V7.57019L15.1571 3ZM6.63636 4.63636H13.1818V7.90909C13.1818 8.81283 13.9144 9.54545 14.8182 9.54545H18.0909V19.3636H6.63636V4.63636ZM14.8182 7.90909V4.97527L17.752 7.90909H14.8182Z" /></svg>',
			children: [
				{
					type: 'input',
					attributes: {
						accept: 'text/plain',
						onchange: 'FileController.getPost(this.files[0])',
						type: 'file',
						title: LAB.bt.loadPost
					}
				}
			]
		},
		{
			type: 'span', 
			attributes: {
				class: 'separator'
			}
		},
		{
			type: 'button',
			attributes: {
				onclick: "View.toggleTheme()",
				title: LAB.bt.toggleTheme 
			},
			html: '<svg viewBox="0 0 24 24"><path d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm0 4c-4.418 0-8 3.582-8 8s3.582 8 8 8v-16z"/></svg>'
		},
		{
			type: 'button',
			attributes: {
				onclick: "View.toggleLayout()",
				title: LAB.bt.toggleLayout 
			},
			html: '<svg viewBox="0 0 512 512"><rect x="48" y="48" width="176" height="176" rx="20" ry="20" /><rect x="288" y="48" width="176" height="176" rx="20" ry="20" /><rect x="48" y="288" width="176" height="176" rx="20" ry="20" /><rect x="288" y="288" width="176" height="176" rx="20" ry="20" /></svg>'
		},
		{
			type: 'button',
			attributes: {
				onclick: "window.open('./help." + LANG + ".html')",
				title: LAB.bt.doc
			},
			html: '<svg viewBox="0 0 24 24"><path d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm1.25 17c0 .69-.559 1.25-1.25 1.25-.689 0-1.25-.56-1.25-1.25s.561-1.25 1.25-1.25c.691 0 1.25.56 1.25 1.25zm1.393-9.998c-.608-.616-1.515-.955-2.551-.955-2.18 0-3.59 1.55-3.59 3.95h2.011c0-1.486.829-2.013 1.538-2.013.634 0 1.307.421 1.364 1.226.062.847-.39 1.277-.962 1.821-1.412 1.343-1.438 1.993-1.432 3.468h2.005c-.013-.664.03-1.203.935-2.178.677-.73 1.519-1.638 1.536-3.022.011-.924-.284-1.719-.854-2.297z"/></svg>'
		}
	]
});

UI.output = chess({
	type: 'article',
	attributes: {
		class: 'out'
	},
	children: [
		{
			type: 'h1',
			attributes: {
				id: "out-h1"
			}
		},
		{
			type: 'div',
			attributes: {
				id: "out-intro"
			}
		}
	]
});

UI.input = chess({
	type: 'article',
	attributes: {
		class: 'in'
	},
	children: [
		{
			type: 'div',
			attributes: {
				class: 'config'
			},
			children: [
				{
					type: 'label',
					children: [
						{
							type: 'input',
							attributes: {
								id: 'post-date',
								type: 'date'
							}
						}
					],
					text: LAB.input.date
				},
				{
					type: 'span', 
					attributes: {
						class: 'separator'
					}
				},		
				{
					type: 'label',
					children: [
						{
							type: 'input',
							attributes: {
								id: 'post-draft',
								type: 'checkbox'
							}
						}
					],
					text: LAB.input.draft
				}
			]
		},
		{
			type: 'div',
			attributes: {
				class: 'content'
			},
			children: [
				{
					type: 'label',
					attributes: {
						for: 'in-desc'
					},
					text: LAB.input.desc
				},
				{
					type: 'input',
					attributes: {
						id: 'in-desc',
						placeholder: LAB.input.desc,
						maxlength: 255,
						title: LAB.input.desc
					}
				},
				{
					type: 'label',
					attributes: {
						for: 'in-h1'
					},
					text: LAB.input.h1
				},
				{
					type: 'input',
					attributes: {
						id: 'in-h1',
						onfocus: 'lastActiveSec = this.parentNode',
						oninput: 'View.runEditor(\'in-h1\')',
						placeholder: LAB.input.h1,
						title: LAB.input.h1
					}
				},
				{
					type: 'label',
					attributes: {
						for: 'in-intro'
					},
					text: LAB.input.secIntro
				},
				{
					type: 'textarea',
					attributes: {
						id: 'in-intro',
						onfocus: 'lastActiveSec = this.parentNode',
						oninput: 'View.runEditor(\'in-intro\')',
						placeholder: LAB.input.secIntro,
						title: LAB.input.secIntro
					}
				},
			]
		},
		{
			type: 'button',
			attributes: {
				id: 'bt-add-section',
				onclick: 'View.addSection()'
			},
			text: LAB.bt.addSec
		}
	]
});

UI.body.appendChild(UI.notice);
UI.body.appendChild(UI.dial);
UI.body.appendChild(UI.nav);
UI.main.appendChild(UI.input);
UI.main.appendChild(UI.output);
UI.body.appendChild(UI.main);

document.documentElement.lang = LANG;
document.head.children[6].textContent = LAB.meta.title; // Meta title.
document.head.children[4].setAttribute('content', LAB.meta.desc); // Meta description.

// Keyboard navigation.
document.onkeydown = (e) => {
	
	if(e.code === 'Escape') {
		View.setDial();
	}
	
	// Focus trap (dial).
	if(e.code === 'Tab') {
		if(e.shiftKey) {		
			if(e.target.classList.contains('trap-first')) {
				document.querySelector('.trap-last').focus();
				e.preventDefault();
			}
		
		}
		else {
			if(e.target.classList.contains('trap-last')) {
				document.querySelector('.trap-first').focus();
				e.preventDefault();
			}
		}
	}

	// Shortcuts.
	else if(e.ctrlKey || e.metaKey) {
		if(UI.body.classList.contains('ws-available')) {
			switch(e.code) {
				case 'KeyS':
					LocalController.pushPost();
					e.preventDefault();
					break;
			}
		}
		if(UI.body.classList.contains('serv-available')) {
			switch(e.code) {
				case 'KeyG':
					ApiController.listImages();
					e.preventDefault();
					break;	
				case 'KeyO':
					ApiController.listPosts();
					e.preventDefault();
					break;
				case 'KeyP':
					ApiController.pushPost();
					e.preventDefault();
					break;
			}
		}
	}
	
};

// Reload post and preferences.
if(localStorage) {
	
	UI.body.classList.add('ws-available');

	const wsPost = localStorage.getItem('post');
	if(wsPost !== null) {
		View.setPost(wsPost);
	}
	const wsTheme = localStorage.getItem('theme');
	if(wsTheme === 'dark') {
		View.toDarkTheme();
	}
	const wsLayout = localStorage.getItem('layout');
	if(wsLayout === 'alternate') {
		View.toColumnLayout();
	}			

}

// Connect to the API.
Model.reachApi('/init', null, mess => {
	if(mess === 'granted') {
		UI.body.classList.add('serv-available');
	}
});

window.onresize = () => View.setViewportHeight();
View.setViewportHeight();