# Créer un blog écoresponsable avec WordsMatter

_Rédigez depuis l'Application ; partagez grâce à l'API connectée. WordsMatter est un module de gestion de blog 100% autonome, libre et gratuit. Son efficience en fait une alternative écoresponsable aux Systèmes de Gestion de Contenu traditionnels (CMS)._

![WordsMatter](/app/assets/logo-wordsmatter.svg)

(contenu initialement publié le 03 janvier 2021 sur le blog [Awebsome](https://awebsome.fr))

En janvier 2020, j'officialisai la sortie de WordsMatter, **un éditeur de blog cross device**.

En diffusant librement le code source de ce travail afin que chacun puisse se l'approprier, j'apportais mon soutien à la **sobriété** et à la **diversité numérique** pour lesquelles je milite avec Awebsome. L'objectif était en effet d'offrir **une alternative pertinente** aux Systèmes de Gestion de Contenu (CMS), souvent plébiscités pour de mauvaises raisons.

Plus d'un an après, WordsMatter sort en version 3.  

Une version plus mature que jamais qui prétend être la dernière. 
Vous cherchez à ajouter espace de blog écoresponsable à un site web professionnel classique ?  

Découvrez WordsMatter, **la solution Awebsome adaptée** à cet enjeu.

## Au sommaire de cet article

* Le projet WordsMatter
	* Pourquoi avoir créé WordsMatter ?
	* WordsMatter est-il un CMS ou un générateur de sites statiques ?
	* Le concept Application/API WordsMatter
	* Quand utiliser WordsMatter ?
	* Arguments de poids, solution ultra légère
	* La dernière version ?
* WordsMatter : identité et super pouvoirs
	* Une interface... Au service de l'utilisateur
	* Une journée (extra)ordinaire avec WordsMatter
	* (bien) Rédiger grâce à l'Application
* Installer WordsMatter
	* Qui peut installer WordsMatter ?
	* Obtenir
	* Langages et technologies
	* Premiers pas
	* Notes relatives à la sécurité
	* Configuration
	* Licence
	* Garantie et support
* WordsMatter suffit-il à créer un blog écoresponsable ?
* Quelques références de blogs WordsMatter

## Le projet WordsMatter

## Pourquoi avoir créé WordsMatter ?

C'était **un besoin récurrent** des clients Awebsome : pouvoir mettre en ligne du contenu rédactionnel sur le site web écoresponsable qu'ils envisagent.

Encore aujourd'hui et pour de nombreux prestataires, il s'agit d'un gap infranchissable, de **la fonctionnalité rédhibitoire** qui interdit de proposer un site web codé en dur alors qu'absolument tout le reste s'y prête. S'orienter alors vers un Système de Gestion de Contenu (ou CMS / Content Management System, en anglais) est un choix facile, mais non pertinent. 

Il en résulte :

* Une consommation excessive en données et en énergie.
* Des lenteurs et des instabilités.
* Des vulnérabilités.

Autant d'aspects qui dégradent de manière générale l'expérience utilisateur des exploitants et des consommateurs du Web (UX).
Début 2021, [40% du Web est propulsé par WordsPress seul](https://w3techs.com/blog/entry/40\_percent\_of\_the\_web\_uses\_wordpress) (en). Et bien d'autres reposent sur des solutions concurrentes au premier. **Je n'en veux point aux CMS d'être ce qu'ils sont**. Que l'on offre à chacun les moyens de s'exprimer librement sur Internet est fondamental. Qu'ils soient utilisés à tort et à travers est en revanche problématique, à plus forte raison quand le choix vient du développeur. C'est même contraire à l'effort qui est attendu de nous, acteurs de la création numérique responsable, dans le cadre du **déploiement de services en ligne raisonnés**.

Et c'est ce qui a donné naissance à WordsMatter.

## WordsMatter est-il un CMS ou un générateur de sites statiques ?

Vous l'aurez compris : **WordsMatter n'est pas un CMS à proprement parler**. Il ne propose ni interface d'administration isolée ni possibilités infinies aux utilisateurs finaux qui utiliseront le produit configuré par leur(s) technicien(s) respectif(s). Je viens pourtant de l'écrire, WordsMatter existe pour faire ce que les CMS font depuis trop longtemps : **donner la possibilité à chacun d'alimenter un blog sans risquer de casser quoi que ce soit d'autre au passage.**

La philosophie du projet se rapproche assez de celle des générateurs de sites statiques. Par défaut, il s'agit d'une solution chargée de produire du **contenu statique** servi en tant que tel (comprenez par là, sous forme de fichiers HTML) avec, dans le même temps, la tenue d'un **sommaire** pouvant servir de point d'entrée aux utilisateurs et la génération du **sitemap** associé. 

Au-delà de cela, **WordsMatter est unique en son genre**.

## Le concept Application/API WordsMatter

### Rédigez depuis l'Application...

Peu importe votre équipement de prédilection, l'Application WordsMatter est **un éditeur de contenu web cross device et ultra performant** capable de produire des articles de blog respectueux des principes d'accessibilité.

### ...publiez grâce à l'API !

Oubliées, les séquences d'authentification fastidieuses : copiée sur le serveur du blog à alimenter, l'API WordsMatter permet d'**en gérer le contenu de façon intuitive** depuis l'Application, sans mot de passe. 

## Quand utiliser WordsMatter ?

C'est bien simple : vous devriez envisager WordsMatter pour créer un blog (écoresponsable ou non) dès lors qu'écrire et publier des articles sont **les seules fonctionnalités utiles** à l'exploitant final d'un service numérique. 

D'où l'importance du dialogue / de la remise en question.

Il est difficile de poser des limites, d'oser (se) questionner. Cela fait pourtant toute la différence. Se fier à ce que l'on croit et foncer tête baissée est précisément ce pour quoi l'on met des outils disproportionnés entre de mauvaises mains.

Interrogez vos clients.  
Interrogez-vous. 

Un exemple parmi tant d'autres : le propre d'un restaurateur est d'être dans sa cuisine, à satisfaire les clients qui affluent dans son établissement. Quel profit peut-il trouver à revoir la couleur ou la police d'écriture du site vitrine qui lui été livré, et qui répondent à des critères dépassant sa propre expertise (principes d'accessibilité, dans le cas présent) ? Son site web devrait être la dernière de ses préoccupations. Et **s'il doit pouvoir y travailler quelque chose, eh bien, que cela soit justifié**.

Trop caricatural ? C'est du vécu !

L'objet de ce rappel est d'enfoncer le clou. Peu importe si votre attrait initial pour le projet penche plutôt vers la **performance**, la **stabilité** ou la **maîtrise énergétique** de votre activité en ligne ;  **vous pourriez compter sur WordsMatter bien plus souvent que vous ne le croyez**.

## Arguments de poids, solution ultra légère

WordsMatter est le fruit d'un développement raisonné à bien des égards. Qu'il s'agisse de la façon dont il a été conçu pour **limiter sa consommation énergétique / données côté client et côté serveur** ou de la façon dont il **encourage ses utilisateurs à produire du contenu plus léger et plus accessible**, le projet s'inscrit dans une démarche de sobriété numérique.

Ses arguments :

* WordsMatter **répond strictement à un besoin** : permettre à tout type de rédacteurs d'alimenter un blog  avec un grand confort d'utilisation. Son efficience le rend **plus fiable**, **plus rapide** et **bien plus neutre pour l'environnement** à l'usage que toute autre solution équivalente.
* L'Application WordsMatter fonctionne en **mode hors-ligne** sur la majorité des navigateurs web modernes et son **interface responsive** lui permet d'être exploité sur une large variété de terminaux (largeur et hauteur minimales recommandées : 360 pixels).
* WordsMatter est **ultra léger** : Application + API + documentations (fr/en) font **à peine plus de 100ko**. Vous lui trouverez forcément une place.
* WordsMatter n'utilise **ni dépendance logicielle ni base de données**. De fait, son intégrité est avant tout liée à ses utilisateurs plutôt qu'à des flux de développement tiers.
* WordsMatter est traduit en **français**, en **anglais**, et est aussi **accessible** que possible.
* WordsMatter est **libre de droits** et **distribué gratuitement**.
Avec de tels points forts, il est facile de comprendre à quel point **WordsMatter est passe-partout**. Sa légèreté et son autonomie le rendent facile à intégrer ici ou là. D'ailleurs : il y a vraiment peu de chance de trouver un environnement qui lui soit _hostile_.

## La dernière version ?

Oui, la version 3 est bel et bien la dernière version majeure de WordsMatter.  

Non, **cela ne devrait pas vous empêcher de l'utiliser en production**. 
Ayant été pensé pour répondre à un besoin spécifique, l'objectif de WordsMatter n'a jamais été de couvrir tous les cas d'usage possibles. Si je parle de version finale, c'est que je suis allé au bout du chemin que je m'étais tracé. **Poursuivre serait contraire à mes intentions de départ.**

D'après mon expérience, les possibilités natives désormais offertes par WordsMatter sont bien suffisantes pour l'usage qui l'attend. Bien sûr, il demeure parfois des exigences spécifiques. **Il nous (vous) revient alors de travailler WordsMatter en ce sens afin qu'il réponde à nos (vos) propres impératifs.** Vous l'aurez peut-être remarqué :  en fonction des thématiques abordées, le blog Awebsome arbore des thèmes de couleurs différentes. C'est l'une des options que j'ai intégrées pour mes propres besoins. Et il m'a déjà été donné d'ajouter, pour divers clients, la gestion du thème sombre, le support des commentaires (avec modération préalable), ou encore la possibilité de sauvegarder le blog entier d'un simple clic (option bien pratique, vous en conviendrez).

Encore une fois, **la seule limite est votre imagination**.

Quid des correctifs ? Ils seront apportés dès lors qu'ils permettent de consolider cette base de WordsMatter. D'ailleurs, si vous découvrez un bug, une faille ou remarquez une coquille quelque part, [merci d'ouvrir une nouvelle Issue sur GitLab](https://gitlab.com/julien.wilhelm/WordsMatter/-/issues). L'expertise de chacun sera toujours appréciée sur les Issues non résolues.

## WordsMatter : identité et super pouvoirs

À ce stade de votre lecture, vous devriez avoir compris l'intérêt de WordsMatter : **propulser un blog à peu près n'importe où**. C'est déjà super, mais de vous à moi, WordsMatter vaut (encore) mieux que cela.

## Une interface... Au service de l'utilisateur

Pour commencer, l'interface utilisateur de l'Application WordsMatter va autant que possible dans le sens de la **simplicité** et de l'**accessibilité**, ce qui ne l'empêche pas de satisfaire **des attentes modernes**, pourvu qu'elles soient légitimes.

* Soignée, elle dispose d'un **thème clair**, lisible en extérieur, et d'un **thème sombre**, plus reposant pour les yeux dans l'obscurité.
* Intuitive, elle se veut **navigable au clavier** et propose quelques **raccourcis** au bénéfice de la **productivité**.
* Malléable, elle propose un affichage en ligne ou en colonne et la prévisualisation du résultat final peut être affinée en appliquant une **feuille de styles personnalisée**. 

Rédigez depuis l'espace de rédaction ; l'espace de prévisualisation livre, **en temps réel**, un aperçu des mises en forme générées. L'Application WordsMatter est très réactive ; vous attendrez rarement sur elle. 

## Une journée (extra)ordinaire avec WordsMatter

Pour estimer WordsMatter à sa juste valeur, plaçons-le maintenant dans un contexte d'utilisation qui pourrait être le vôtre. 

Imaginez un peu : 

1. Une idée d'article vous vient au réveil. Vous ouvrez l'Application WordsMatter d'une main sur votre mobile, saisissez quelques mots clés qui vous tiendront lieu de feuille de route. En trois pressions, **vous envoyez le brouillon vers l'API** pour en différer la rédaction.
2. Plus tard, dans la journée, parfaitement installé (ou non) devant votre ordinateur portable, **vous rapatriez votre brouillon** à l'identique de l'API vers l'Application. Il vous faut faire des recherches pour approfondir votre sujet. Pour davantage de confort, vous scindez votre écran en deux. Après tout, WordsMatter se suffit de peu.
3. Vous faites plusieurs pauses méritées. En vous dégourdissant les jambes, malheur : vous arrachez par mégarde le câble d'alimentation du laptop sur lequel vous travailliez ! Écran noir. Vous vous jetez sur la prise électrique, redémarrez votre système. Les secondes les plus longues de votre vie. Vous avez des sueurs froides à l'idée d'avoir perdu votre travail. Miracle cependant : **votre post est restauré depuis la mémoire locale de votre navigateur Web**. Tant qu'elle n'est pas supprimée depuis l'Application ET que vos données de navigation sont conservées, la dernière sauvegarde est rappelée à chaque ouverture de WordsMatter, **même si le terminal a été mis hors tension entre temps**. Touches CTRL + S : un raccourci précieux, à consommer sans modération dans WordsMatter. 
4. Vous publiez enfin votre post. Par acquit de conscience, vous vous rendez sur votre blog pour vous assurer de son rendu. Le résultat est **tel que vous l'espériez**. Quant aux analyses de performances techniques, l'on peut difficilement faire mieux.
5. Dans la soirée, tandis que vous regardez une série Netflix sur votre tablette, vous comprenez tout à coup avoir écrit quelque chose à même de discréditer votre expertise. Ni une ni deux, **vous accédez à WordsMatter de votre tablette, sans quitter votre lit, et publiez dans la foulée un correctif**. Votre réputation est sauve (et vos pieds, bien au chaud).
6. (Épilogue) Quelqu'un vous propose de vous racheter ce fantastique article pour le publier en son nom. Qu'à cela ne tienne : qu'il s'agisse d'utiliser la version WordsMatter d'un ami ou de faire des copies locales de ses brouillons, **WordsMatter permet l'import et l'export de post sous forme de fichier**.

Cette fiction, qui fait le tour des fonctionnalités de WordsMatter, témoigne surtout de sa plus grande force. Qu'un post soit prêt à être diffusé au monde entier ou doive demeurer à l'état de brouillon privé, **il est très intuitif de pousser son contenu depuis l'Application vers l'API et de reprendre plus tard son édition depuis un tout autre terminal configuré.**

Le contenu, justement : parlons-en.

## (bien) Rédiger grâce à l'Application

### Logique sémantique

L'Application WordsMatter encourage ses utilisateurs à s'inscrire dans **une démarche vertueuse de respect de la logique sémantique HTML**. Cette approche profite à certaines personnes en situation de handicap et plaît tout particulièrement aux moteurs de recherche, ces derniers préférant en effet faire remonter dans leur classement du **contenu structuré** plutôt qu'une tambouille qui leur est inintelligible.

Ainsi :

* Le titre principal d'un post (h1) ne peut être renseigné qu'à un seul endroit.
* Les titres de sections (h2) ne peuvent être ajoutés qu'en en-tête des sections qu'ils introduisent.
D'autres mises en forme sont utilisables plus librement par défaut :
* Titres de niveau 3 à 6 (h2-h6).
* Texte important (strong).
* Texte emphasique (em).
* Listes ordonnées (ol).
* Listes non ordonnées (ul).
* Liens hypertextes (a).
* Images (img).
* Figures (figure).

Bien assez pour retranscrire avec richesse nos pensées. 

### La syntaxe WordsMatter

La version 3 de WordsMatter signe l'arrivée d'**une nouvelle syntaxe de mise en forme** que vous pouvez découvrir en détail dans la documentation utilisateur de l'Application. L'ancienne souffrait de sa complexité. Sa remplaçante se veut **plus lisible**, **plus facile** et **plus rapide** à utiliser.
 
On m'a parfois reproché de ne pas avoir utilisé [Markdown](https://daringfireball.net/projects/markdown/) (en) ; j'ai en partie tenu compte de cette suggestion, puisque la version finale de WordsMatter en réutilise maintenant certains codes, tout en apportant **ses propres subtilités**.

Pourquoi une syntaxe mixte ?  

Pour diverses raisons.

En Markdown, il est par exemple nécessaire de taper six dièses (#) suivis d'un espace pour introduire un titre de niveau 6. Cela fait 7 caractères au total, contre 2 avec la syntaxe WordsMatter pour le même résultat. Je souhaitais aussi **simplifier le travail des expressions régulières**, qui ont à charge de convertir le contenu brut en HTML sous le capot. Et ne pas effrayer les utilisateurs moins techniques avec une trop grande diversité de caractères spéciaux. Cela explique en partie pourquoi le tiret bas (\_) revêt autant d'importance.

### Une image vaut mieux que mille mots (ou pas)

Cette dernière version de WordsMatter est aussi l'occasion d'introduire **la gestion des images au sein même de l'Application**.
Auparavant, il fallait télécharger par soi-même ses médias sur son serveur avant de pouvoir y faire référence. S'agissant d'une opération technique, le processus pouvait rebuter, à juste titre.

Désormais, sous réserve de connexion à l'API, **les tâches d'envoi et de suppression d'images peuvent être accomplies avec facilité depuis la galerie de L'Application**, où toutes sont référencées.

J'ai longtemps repoussé l'intégration de cette fonctionnalité, dont l'absence était un handicap. On dit qu'une image vaut mieux que mille mots. Or, **dans le monde numérique, une image pèse mille fois plus que les mots et véhicule bien moins d'informations qu'il n'y paraît.** Le traitement des images côté serveur et client, multiplié par leur nombre conséquent, alimente un gouffre énergétique sans fin. 
Ce média aujourd'hui trop répandu reste toutefois utile, voilà pourquoi j'ai tranché en sa faveur. 

Mais pas n'importe comment ! 

Au-delà des enjeux environnementaux liés à l'explosion de la consommation de bande passante, utiliser **une stratégie de diffusion d'images** adaptées à la résolution d'écran du client est capital pour améliorer l'expérience utilisateur de son site web (réduction des temps de chargement). Et, par extension, son référencement naturel.

Le Web s'est outillé pour répondre à ces défis. En bon élève, WordsMatter se devait d'en tirer parti.

Ainsi :

1. Dans un premier temps, les images sont systématiquement **dupliquées**, **redimensionnées** et **compressées** côté serveur lors de l'envoi depuis l'Application.
2. Dans un second temps, lorsqu'un post publié contient des images issues de la galerie, les différents formats générés à l'envoi sont injectés par l'API dans un élement _picture_ et ses enfants _source_ pour servir du contenu dit _responsive_. Les images générées portent aussi l'attribut _loading_, valeur _lazy_, afin de  différer au moment opportun leur chargement dans la page ([support de l'attribut "loading" sur Can I Use](https://caniuse.com/?search=loading) (en)).

En clair : **le navigateur de l'utilisateur détermine quand et quelles ressources optimisées seront à charger sans que personne n'ait rien à faire.**
Des limitations ont toutefois été fixées pour éviter les abus (formats de fichier supportés, taille maximale autorisée, pas d'envoi simultané) ; bien qu'il soit aisé de contourner ces garde-fous, il est dans l'intérêt de tous de les conserver, sinon de les renforcer. 

## Installer WordsMatter

Les éléments suivants sont techniques. Si vous le préférez, vous pouvez vous rendre en conclusion de l'article, et plus précisément à : "WordsMatter suffit-il à créer un blog écoresponsable ?"

## Qui peut installer WordsMatter ?

Dans l'absolu, WordsMatter est **simple** et **prêt à l'emploi**. Mais **il doit être configuré avec soin**.

Si vous êtes à l'aise avec l'informatique, connaissez les concepts fondamentaux du Web et lisez avec attention les consignes ci-dessous, il est fort probable que vous saurez mettre WordsMatter en ligne par vos propres moyens. Dans le cas contraire, faites-vous accompagner  ([par Awebsome, par exemple](https://awebsome.fr)).

Par défaut, WordsMatter est livré sans thème (texte noir sur fond blanc pour le blog généré). Ce n'est pas gênant en soi, mais vous souhaiterez certainement changer cela. À cette fin, la maîtrise des langages HTML et CSS est impérative.

## Obtenir

WordsMatter est en téléchargement libre depuis ce dépôt GitLab. 

## Langages et technologies

* Côté client, l'Application WordsMatter repose sur des langages intégrés aux navigateurs web : HTML, CSS et JavaScript.
* Côté serveur, l'API WordsMatter est en PHP natif ; un serveur Apache est requis pour son fonctionnement.

## Premiers pas

[Téléchargez l'archive du dépôt WordsMatter](https://gitlab.com/julien.wilhelm/WordsMatter), à décompresser sur votre terminal.

* L'Application WordsMatter dépend du dossier app. **Attention : quiconque accède à une version donnée de l'application WordsMatter peut se connecter à l'API qui lui aura été attribuée. L'application ne doit donc en aucun cas être hébergée sur le serveur web où le blog est alimenté**.
* L'API WordsMatter dépend uniquement du dossier api. **Ce dossier est à placer en lieu sûr sur le serveur web destinataire**.
Pour lancer l'Application WordsMatter en **mode déconnecté** (fonctionnalités limitées), ouvrez le fichier index.html du dossier app dans votre navigateur web. Sur certains systèmes d'exploitation mobiles, scripts et styles peuvent être désactivés par défaut. Ce comportement rendant l'Application WordsMatter inopérationnelle, il faut alors passer par le chemin réel du fichier, relatif au média de stockage.
Pour lancer l'Application WordsMatter en **mode connecté**, atteignez ce même point d'entrée depuis un serveur web local Apache supportant PHP. Les dossiers et fichiers nécessaires à la vie du blog seront alors créés et toutes les fonctionnalités d'exploitation du blog seront débloquées. 

## Notes relatives à la sécurité

### Protocole HTTPS

L'Application et l'API sont prévues pour échanger dès lors que leurs identifiants concordent. Parce que l'identifiant de l'Application transite en clair, il est recommandé d'utiliser WordsMatter uniquement dans un contexte HTTPS afin de limiter le risque d'interception d'informations lors de la connexion avec l'API.

### Cross-origin resource sharing

Afin qu'Application et API puissent échanger, les requêtes CORS doivent être autorisées côté serveur. L'API est configurée pour envoyer l'entête "Access-Control-Allow-Origin" adéquat au client.

### Protection de l'API

Pour se prémunir des attaques de force brute, il est vivement conseillé de renommer le dossier api. Ce nom de dossier devant constituer une URL valide, certains caractères sont exclus d'office ; le mieux est de combiner minuscules, majuscules et chiffres dans la longueur. Par exemple, un intitulé de 50 caractères tel que "KguZ5n9vnXh4saDpe65LcmgzybjLcTG9wdqveEkwWvFx5eZLpw" pourrait donner lieu à une URL imprévisible telle que "https://votre-site-.fr/KguZ5n9vnXh4saDpe65LcmgzybjLcTG9wdqveEkwWvFx5eZLpw". Pour que cette protection soit efficace, pensez à interdire l'exploration de l'arborescence du site (directive Options -Indexes ou autre à la racine de votre serveur) !

## Configuration

Avant d'utiliser WordsMatter en production, il est utile d'intervenir :

* Dans les fichiers "/app/assets/config.js" et "/api/config.php" qui contiennent différentes variables de configuration.
* Dans les fonctions "buildPublicList" et "exportPost" qui décrivent des templates en fin de fichier "/api/Post.php".

### /app/js/config.js

Il contient : 

* EDITOR_ID : un identifiant unique et complexe à attribuer à votre application WordsMatter.
* API_URL : l'adresse distante (et secrète) de l'API WordsMatter à utiliser.
* LANG : la langue de l'interface utilisateur de l'Application WordsMatter ('fr' pour français, 'en' pour anglais). 

### /api/config.php

Il contient notamment : 

* EDITOR_ID : doit être égale à la constante du même nom de l'Application à associer.
* DOMAIN : l'URL à laquelle est hébergé le blog.

### Fonction buildPublicList (/api/Post.php)

C'est le gabarit par défaut du sommaire, reconstruit dès lors qu'un post est publié / supprimé depuis l'Application. Simpliste et dépourvu de CSS, c'est un bon point de départ pour créer un blog écoresponsable.

### Fonction exportPost (/api/Post.php)

C'est le modèle de page par défaut auquel est injecté le HTML valide lors de la publication d'un post. Comme pour le fichier précédent, il est livré sans styles.

...

Pour le reste, il faudra creuser ! Le projet comporte au final **peu de fichiers** ; tous les scripts et fonctions ont **un intitulé explicite** ; le code source est **commenté** lorsque cela est vraiment nécessaire. Votre exploration devrait s'en trouver facilitée. 

## Licence

Dans l'intérêt de l'**efficience**, de l'**inclusion** et de la **sobriété numérique**, [WordsMatter est proposé sous licence GNU GPL version 3 ou ultérieure](https://www.gnu.org/licenses/gpl-3.0.en.html).
Pour faire simple, cela signifie que **vous pouvez faire ce que vous voulez de WordsMatter** y compris le commercialiser (ou pas), en y ayant apporté des modifications (ou pas). Cette liberté qui vous est offerte, aussi grande soit-elle, ne devrait pas vous faire perdre de vue la raison d'être de WordsMatter (mais vous pouvez).

## Garantie et support

WordsMatter est proposé en l'état et sans garantie quant à son fonctionnement par Julien Wilhelm ([Awebsome](https://awebsome.fr)). S'agissant d'**un travail bénévole**, il est de la responsabilité de chacun de prendre les dispositions qui s'imposent pour garantir l'intégrité de son travail. Tout dysfonctionnement lié de près ou de loin à WordsMatter ne saurait être reproché à son créateur. Par ailleurs, le support client n'est assuré par Awebsome que pour les clients Awebsome. Si WordsMatter vous a été installé par un autre prestataire, merci d'adresser vos questions à ce dernier.

## WordsMatter suffit-il à créer un blog écoresponsable ?

Comme j'aimerais répondre par l'affirmative !  
La réalité est plus variable. 

**WordsMatter est livré avec de bonnes intentions**. Ces intentions, aussi louables soient-elles, peuvent être corrompues à un moment ou un autre par un déploiement inadapté ou par l'usage absurde qui en est fait derrière. Un outil n'est jamais qu'un moyen que l'on plie à sa volonté. Parfois, c'est regrettable, l'outil est poussé dans une direction qui ne lui était pas destinée.
Avec Awebsome, je m'efforce de donner toutes les clés à mes clients pour m'assurer qu'ils respecteront le site web écoresponsable que je leur aurais créé. Il en va de même quand j'utilise WordsMatter, à bon escient, pour répondre à leur demande.

Créer un blog écoresponsable ?  
**WordsMatter est FAIT pour ça.**

Respectez sa philosophie, et vous pourrez sans peine prétendre que votre blog, à travers son approche de sobriété fonctionnelle, fait mieux que le reste de l'univers (pour un peu que vous proposiez du contenu de qualité à vos utilisateurs, vous partiriez même d'un très bon pied côté référencement naturel).
Plus qu'**un commun numérique** dont nous pouvons nous réjouir, WordsMatter est un rempart contre l'obésité des logiciels qui fatigue nos équipements et en accélère l'obsolescence. Plus qu'une alternative, c'est **une réponse enfin adaptée** à notre vœu le plus cher : faire entendre votre voix dans le monde entier, sans faire taire la planète.

## Quelques références de blogs WordsMatter

Je le dis et je le redis : les clients Awebsome sont très souvent engagés sur le plan environnemental. Il était donc prévisible que WordsMatter soit accueilli à bras ouverts comme solution adéquate à leur besoin de blog écoresponsable pour leur activité professionnelle à impact positif. Ils ont fait un choix audacieux. Et de la même manière qu'ils ont été satisfaits de mon travail, WordsMatter les a emballés.

Quelques exemples :

* [https://solastalgie-merci.fr/](https://solastalgie-merci.fr/)
* [https://elenavivaldi.com/blog/](https://elenavivaldi.com/blog/)
* [https://clinique-travail.fr/actualites/](https://clinique-travail.fr/actualites/)
* [https://aelan.fr/blog](https://aelan.fr/blog)

WordsMatter est en téléchargement libre ; en bon challenger, il a tout ce qu'il faut pour faire sa vie de son côté.